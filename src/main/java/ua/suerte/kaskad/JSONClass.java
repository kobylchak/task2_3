package ua.suerte.kaskad;

import lombok.Data;

@Data
public class JSONClass {
    private boolean success;
    private String terms;
    private String privacy;
    private int timeStamp;
    private String source;
    private Quotes quotes;

    public JSONClass() {
    }

    public JSONClass(boolean success, String terms, String privacy, int timeStamp, String source, Quotes quotes) {
        this.success = success;
        this.terms = terms;
        this.privacy = privacy;
        this.timeStamp = timeStamp;
        this.source = source;
        this.quotes = quotes;
    }
}
