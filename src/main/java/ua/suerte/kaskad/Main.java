package ua.suerte.kaskad;

import java.io.*;
import java.net.*;

import com.google.gson.*;

public class Main {
    public static void main(String[] args) throws Exception {
        String request = "http://apilayer.net/api/live?access_key=4dab7bf6f97d834fe1ad4274c49dea20&currencies=EUR,UAH&format=1";
        String result = performRequest(request);
        System.out.println(result);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JSONClass json = gson.fromJson(result, JSONClass.class);
        System.out.println("USDEUR - " + json.getQuotes().getUSDEUR());
        System.out.println("USDUAH - " + json.getQuotes().getUSDUAH());
    }

    private static String performRequest(String urlStr) throws IOException {
        URL url = new URL(urlStr);
        StringBuilder sb = new StringBuilder();
        HttpURLConnection http = (HttpURLConnection) url.openConnection();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream()));
            char[] buf = new char[1000000];
            int r = 0;
            do {
                if ((r = br.read(buf)) > 0)
                    sb.append(new String(buf, 0, r));
            } while (r > 0);
        } finally {
            http.disconnect();
        }
        return sb.toString();
    }
}