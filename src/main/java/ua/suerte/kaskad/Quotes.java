package ua.suerte.kaskad;

import lombok.Data;

@Data
public class Quotes {
    private double USDEUR;
    private double USDUAH;

    public Quotes() {
    }

    public Quotes(double USDEUR, double USDUAH) {
        this.USDEUR = USDEUR;
        this.USDUAH = USDUAH;
    }
}
